#!/usr/bin/perl

#El servidor/version de Perl no intepreta la estructura Switch

print "Content-type: text/html\n\n";

$num = <STDIN>;
$num = substr ($num, 7);
chop ($num);
chop ($num);

print "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
print "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n";
print "<head>\n";
print "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />\n";
print "<title>P&aacute;gina generada din&aacute;micamente con Perl</title>\n";
print "<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/estilos.css\" />\n";
print "</head>\n";
print "<body>\n";
if (validNumber($num)) {
    print '<h1>Ud. ingresó: ' . $num . '</h1><br/>' . "\n";
    print "<p><b>Aiken: </b></p><div class='container'>";
    printAiken($num); 
    print "</div>\n";
    print "<p><b>Stibitz: </b><p><div class='container'>";
    printStibitz($num);
    print "</div>\n";
    printf('<p><b>Biquinario:</b></p> <div class="container">');
    printBiquinario($num);
    printf('</div><br/>');
} else {
    printf("El número no es válido\n");
}
    
print '<br/><a href="../" ><button>Volver</button></a><br/>' . "\n";
print "</body>\n";
print "</html>\n";



sub validNumber {
    my ($number) = @_;
    $number =~ m/^[\d]+$/;
}


sub printBiquinario {
    my ($number) = @_;
    @chars = split("", $number);
    for ($i=0; $i < length($number); $i++) {
        if ($chars[$i] =~ m/\d/) {
            printf('<div class="digitImage">');
            printf('<img class="biquinario" src="../img/' . $chars[$i] . '.png" title="' . $chars[$i] . '" alt="' . $chars[$i] . '"/>' . "\n");
            printf('</div>');
        }
    }
}

sub printStibitz {
    my ($number) = @_;
    my @stibitz = ('0011', '0100', '0101', '0110', '0111', '1000', '1001', '1010', '1011', '1100');
    @char = split("", $number);
    for ($i=0; $i < length($number); $i++) {
        if ($char[$i] =~ m/\d/) {
            printf('<div class="digitText">');
            print($stibitz[$char[$i]]);
            printf('</div>');
        }
    }
}


sub printAiken {
    my ($number) = @_;
    my @aiken = ('0000', '0001', '0010', '0011', '0100', '1011', '1100', '1101', '1110', '1111');
    @char = split("", $number);
    for ($i=0; $i < length($number); $i++) {
        if ($char[$i] =~ m/\d/) {
            printf('<div class="digitText">');
            printf($aiken[$char[$i]]);
            printf('</div>');
        }
    }
}